from __future__ import division
import math
from django.core.management.base import BaseCommand, CommandError
import datetime
import time
import csv
import StringIO
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from payments.models import Subscription, Receipt
from django.core.mail import EmailMessage
from django.db.models.loading import get_model
from report_builder.models import Report
from payments.utils import charge_subscription
from report_builder.views import report_to_list
from django.conf  import settings
import signal
import subprocess, threading
from participants.models import Participant

class Command(BaseCommand):
    args = ""
    help = ""


    def handle(self, *args, **options):
        print "Entered"
        pk = args[0]
        user = Participant.objects.get(pk=args[1])
        import cStringIO as StringIO
        from openpyxl.workbook import Workbook
        from openpyxl.writer.excel import save_virtual_workbook
        from openpyxl.cell import get_column_letter
        from django.core.mail import EmailMessage
        import re
        #time.sleep(360)
        report = get_object_or_404(Report, pk=pk)
        success_url = './'

        wb = Workbook()
        ws = wb.create_sheet()
        ws.title = re.sub(r'\W+', '', report.name)[:30]
        filename = re.sub(r'\W+', '', report.name) + '.xlsx'

        i = 0
        for field in report.displayfield_set.all():
            cell = ws.cell(row=0, column=i)
            cell.value = field.name
            cell.style.font.bold = True
            ws.column_dimensions[get_column_letter(i+1)].width = field.width
            i += 1
        user = user
        objects_list, message = report_to_list(report, user)
        for row in objects_list:
            try:
                ws.append(row)
            except ValueError as e:
                ws.append([e.message])
            except:
                ws.append(['Unknown Error'])

        myfile = StringIO.StringIO()
        myfile.write(save_virtual_workbook(wb))
        if settings.DEBUG:
            email_addresses = [user.email, 'admin-report-status@healthywage.com']
        else:
            email_addresses = [user.email, 'admin-report-status@healthywage.com']
        sender = 'HealthyWage Team <info@healthywage.com>'
        msg = EmailMessage(
            subject=report.name,
            body='Report is attached here',
            from_email=sender,
            to=email_addresses
        )
        msg.attach(filename, myfile.getvalue(), 'text/csv')
        msg.send()

class Command1(object):
    def __init__(self, cmd):
        print cmd
        self.cmd = cmd
        self.process = None

    def run(self, timeout):
        def target():
            print 'Thread started'
            self.process = subprocess.Popen(self.cmd, shell=True)
            self.process.communicate()
            print 'Thread finished'

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(timeout)
        if thread.is_alive():
            print 'Terminating process'
            self.process.terminate()
            thread.join()
        print self.process.returncode
